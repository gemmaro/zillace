require 'test_helper'

class ZillaceTest < Test::Unit::TestCase
  test 'VERSION' do
    assert do
      ::Zillace.const_defined?(:VERSION)
    end
  end

  test 'database connect' do
    assert { ::Zillace::Database.new.connect }
  end

  test 'place each' do
    assert { ::Zillace::Place.each.all? { |place| place.instance_variable_get(:@id).is_a?(Integer) } }
    assert { ::Zillace::Place.each.all? { |place| place.url.is_a?(String) } }
    assert { ::Zillace::Place.each.all? { |place| [String, NilClass].any? { |klass| place.title.is_a?(klass) } } }
  end

  test 'bookmark each' do
    assert { ::Zillace::Bookmark.each.all? { |bookmark| [String, NilClass].any? { |klass| bookmark.title.is_a?(klass) } } }
    assert { ::Zillace::Bookmark.each.all? { |bookmark| bookmark.date_added.is_a?(Time) } }
    assert { ::Zillace::Bookmark.each.all? { |bookmark| bookmark.last_modified.is_a?(Time) } }
  end
end
