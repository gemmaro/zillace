# Change log for Zillace

## Unreleased

## 0.0.2 - 2023-12-02

### Fixed

* Runtime dependency name to `sqlite3`.

## 0.0.1 - 2023-12-02

- Initial release
