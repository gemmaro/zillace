require_relative 'zillace/version'
require 'sqlite3'
require 'forwardable'

module Zillace
  def self.prtime(micro_seconds)
    Time.new(Rational(micro_seconds, 1_000_000))
  end

  class Error < StandardError; end

  class Database
    def initialize(profile_prefix: File.join(Dir.home, '.var/app/org.mozilla.firefox'))
      @profile_prefix = profile_prefix
    end

    def connect
      pattern = File.join(@profile_prefix, '.mozilla/firefox/*.default-release/places.sqlite')
      path = Dir[pattern].first
      @database = SQLite3::Database.new(path)
      self
    end

    extend Forwardable
    def_delegators :@database, :execute
  end

  class Place
    attr_reader :url, :title

    def initialize(id:, url:, title:)
      @id = id
      @url = url
      @title = title
    end

    def self.each(database: Database.new.connect, &block)
      enumerator = Enumerator.new do |enumerator|
        database.execute('select id, url, title from moz_places') do |row|
          id, url, title = row
          enumerator << new(id:, url:, title:)
        end
      end

      block_given? ? enumerator.each(&block) : enumerator
    end

    def self.find(id, database: Database.new.connect)
      url, title = database.execute('select url, title from moz_places where id = ?', id).first
      return new(id:, url:, title:)
    end
  end

  class Bookmark
    attr_reader :title, :date_added, :last_modified

    def initialize(fk:, title:, date_added:, last_modified:, database: Database.new.connect)
      @fk = fk
      @title = title
      @date_added = date_added
      @last_modified = last_modified
      @database = database
    end

    def self.each(database: Database.new.connect, &block)
      enumerator = Enumerator.new do |enumerator|
        database.execute('select fk, title, dateAdded, lastModified from moz_bookmarks') do |row|
          fk, title, date_added, last_modified = row
          enumerator << new(fk:, title:, date_added: ::Zillace.prtime(date_added), last_modified: ::Zillace.prtime(last_modified), database:)
        end
      end

      block_given? ? enumerator.each(&block) : enumerator
    end

    def url
      Place.find(@fk, database: @database).url
    end
  end
end
