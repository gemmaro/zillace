require_relative 'lib/zillace/version'

Gem::Specification.new do |spec|
  spec.name = 'zillace'
  spec.version = Zillace::VERSION
  spec.authors = ['gemmaro']
  spec.email = ['gemmaro.dev@gmail.com']

  spec.summary = 'Mozilla Firefox places SQLite3 database library'
  spec.description = 'Zillace is a database manipulation library for Mozilla Firefox.  It reads the places.sqlite file under the user\'s Firefox profile directory.'
  spec.homepage = "https://codeberg.org/gemmaro/zillace"
  spec.required_ruby_version = '>= 3.1'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/gemmaro/zillace.git"
  spec.metadata["changelog_uri"] = "https://codeberg.org/gemmaro/zillace/src/branch/main/CHANGELOG.md"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end

  spec.require_paths = ['lib']

  spec.add_dependency 'sqlite3'
end
