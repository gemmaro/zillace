# This sample script generates bookmark contents for qutebrowser's quickmarks file content
#
# Usage:
#   ruby script/sample.rb >> ~/.config/qutebrowser/quickmarks

require_relative '../lib/zillace'

Zillace::Bookmark.each do |bookmark|
  url = bookmark.url or next
  title = bookmark.title or next
  title = title.lines.join(' ')
  puts [title, url].join(' ')
end
